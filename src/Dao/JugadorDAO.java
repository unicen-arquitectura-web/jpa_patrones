package Dao;

import java.util.List;

import modelo.Jugador;

public interface JugadorDAO {

	public void insertar(Jugador jugador);

	public void actualizar(Jugador jugador);

	public void eliminar(Jugador jugador);

	public Jugador buscar_por_id(int id);

	public List<Jugador> listar();
}