package Dao;

import java.util.List;

import javax.persistence.EntityManager;

import modelo.Jugador;

public class JugadorDAOImplMySQL implements JugadorDAO {
	private EntityManager em;

	public JugadorDAOImplMySQL(EntityManager em) {
		this.em = em;
	}

	public Jugador buscar_por_id(int id_jugador) {

		Jugador jugador = em.find(Jugador.class, id_jugador);

		return jugador;
	}

	@Override
	public void insertar(Jugador jugador) {
		// código para insertar un jugador en la base de datos
	}

	@Override
	public void actualizar(Jugador jugador) {
		// código para actualizar un jugador en la base de datos
	}

	@Override
	public void eliminar(Jugador jugador) {
		// código para eliminar un jugador de la base de datos
	}

	@Override
	public List<Jugador> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * @Override public List<Jugador> listar() { // código para listar todos los
	 * jugadores de la base de datos return null; }
	 */
}
