package modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Jugador {

	@Id
	private int id;

	@Column(nullable = false)
	private String nombre;

	private int puntos;

	private int partidas;

	public Jugador() {
		super();
	}

	public Jugador(int id, String nombre, int puntos, int partidas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.puntos = puntos;
		this.partidas = partidas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	public int getPartidas() {
		return partidas;
	}

	public void setPartidas(int partidas) {
		this.partidas = partidas;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Jugador [id=" + id + ", nombre=" + nombre + ", puntos=" + puntos + ", partidas=" + partidas + "]";
	}

}
